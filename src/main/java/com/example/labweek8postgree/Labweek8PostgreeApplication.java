package com.example.labweek8postgree;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Labweek8PostgreeApplication {

	public static void main(String[] args) {
		SpringApplication.run(Labweek8PostgreeApplication.class, args);
	}

}
