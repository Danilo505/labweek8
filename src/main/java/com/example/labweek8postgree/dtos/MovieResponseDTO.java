package com.example.labweek8postgree.dtos;

public record MovieResponseDTO(
        String id,
        String movieName,
        String description,
        double price){
}