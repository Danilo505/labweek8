package com.example.labweek8postgree.dtos;

public record MovieRequestDTO(
        String movieName,
        String description,
        double price) {
}
