package com.example.labweek8postgree.services;

import com.example.labweek8postgree.dtos.MovieRequestDTO;
import com.example.labweek8postgree.dtos.MovieResponseDTO;
import com.example.labweek8postgree.entities.Movie;
import com.example.labweek8postgree.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieService {
    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }

    public MovieResponseDTO postMovie(MovieRequestDTO movieRequestDTO){
    Movie movie = new Movie(movieRequestDTO);

    if (movieRequestDTO.price() < 0)
        return null;
    movieRepository.save(movie);

    return new MovieResponseDTO(movie.getId(), movie.getMovieName(), movieRequestDTO.description(), movie.getPrice());
    }

    public MovieResponseDTO findById(String id){
       var movie = movieRepository.findById(id).orElseThrow();
       return new MovieResponseDTO(movie.getId(),movie.getMovieName(), movie.getDescription(), movie.getPrice());

    }

    public List<MovieResponseDTO> AllMovies(){
        return convert(movieRepository.findAll());
    }

    public List<MovieResponseDTO> convert(List<Movie> movies){
        return movies
                .stream()
                .map(movie -> new MovieResponseDTO(movie.getId(), movie.getMovieName(), movie.getDescription(), movie.getPrice()))
                .collect(Collectors.toList());
    }
}
