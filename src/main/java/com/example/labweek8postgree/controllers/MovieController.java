package com.example.labweek8postgree.controllers;


import com.example.labweek8postgree.dtos.MovieRequestDTO;
import com.example.labweek8postgree.dtos.MovieResponseDTO;
import com.example.labweek8postgree.services.MovieService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/movie")
public class MovieController {
    private final MovieService movieService;

    public MovieController(MovieService movieService){
        this.movieService = movieService;
    }

    @PostMapping("/create")
    public ResponseEntity<MovieResponseDTO> postMovie(@RequestBody @Valid MovieRequestDTO movieRequestDTO){
        return ResponseEntity.status(HttpStatus.CREATED).body(movieService.postMovie(movieRequestDTO));
    }

    @GetMapping("/{id}")
    public ResponseEntity<MovieResponseDTO> getMovieById(@PathVariable String id){
        return ResponseEntity.ok(movieService.findById(id));
    }

    @GetMapping
    public ResponseEntity<List<MovieResponseDTO>> getAllMovies(){
        return ResponseEntity.ok(movieService.AllMovies());
    }

}

