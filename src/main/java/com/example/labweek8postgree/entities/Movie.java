package com.example.labweek8postgree.entities;

import com.example.labweek8postgree.dtos.MovieRequestDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@Document("movie")
@AllArgsConstructor
@NoArgsConstructor
public class Movie {

    @Field(name = "_id") @MongoId
    private String id;
    @Field(name = "movie_name")
    private String movieName;
    @Field(name = "description_movie")
    private String description;
    @Field(name = "price_movie")
    private double price;

    public Movie(MovieRequestDTO movie) {
        this.movieName = movie.movieName();
        this.description = movie.description();
        this.price = movie.price();
    }
}
