package com.example.labweek8postgree.repositories;

import com.example.labweek8postgree.entities.Movie;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MovieRepository extends MongoRepository<Movie, String> {

}
